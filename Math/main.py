import sympy
print("#Дифференциальные уравнения первого порядка.")
print("#Дифференциальные уравнения с разделяющимися переменными")
print("#Пример 1")

x = sympy.symbols('x')
y = sympy.Function('y')

equation = sympy.Eq(x*y(x).diff(x), y(x))
print("Условие:")
print(equation)
solution = sympy.dsolve(equation)
print("Общее решение:")
print(solution)
print("Ответ сошелся")
print("-----------------------------------------------------------------")

print("#Дифференциальные уравнения первого порядка.")
print("#Дифференциальные уравнения с разделяющимися переменными")
print("#Пример 2")

x = sympy.symbols('x')
y = sympy.Function('y')

equation = sympy.Eq(y(x).diff(x), -2*y(x))
print("Условие:")
print(equation)
solution = sympy.dsolve(equation)
print("Общее решение:")
print(solution)
C1=sympy.solve(solution.subs(x,0).subs(y(0),2),'C1')[0]
z=solution.subs('C1',C1)
print("Частное решение:")
print(z)
print("Ответ сошелся")
print("-----------------------------------------------------------------")
print("#Дифференциальные уравнения первого порядка.")
print("#Дифференциальные уравнения с разделяющимися переменными")
print("#Пример 3")

x = sympy.symbols('x')
y = sympy.Function('y')

equation = sympy.Eq(y(x).diff(x)+(2*y(x)+1)*sympy.cot(x),0)
print("Условие:")
print(equation)
solution = sympy.dsolve(equation)
print("Общее решение:")
print(solution)
print("Ответ сошелся")
print("-----------------------------------------------------------------")
print("#Дифференциальные уравнения первого порядка.")
print("#Дифференциальные уравнения с разделяющимися переменными")
print("#Пример 4")

x = sympy.symbols('x')
y = sympy.Function('y')

equation = sympy.Eq(y(x)*sympy.ln(y(x))+x*y(x).diff(x),0)
print("Условие:")
print(equation)
solution = sympy.dsolve(equation)
print("Общее решение:")
print(solution)
C1=sympy.solve(solution.subs(x,1).subs(y(1),sympy.exp(1)),'C1')[0]
z=solution.subs('C1',C1)
print("Частное решение:")
print(z)
print("Ответ сошелся")
print("-----------------------------------------------------------------")
print("#Дифференциальные уравнения первого порядка.")
print("#Дифференциальные уравнения с разделяющимися переменными")
print("#Пример 5")

x = sympy.symbols('x')
y = sympy.Function('y')

equation = sympy.Eq(y(x).diff(x)*sympy.exp(y(x)-x*x)-2*x,0)
print("Условие:")
print(equation)
solution = sympy.dsolve(equation)
print("Общее решение:")
print(solution)
C1=sympy.solve(solution.subs(x,0).subs(y(0),sympy.ln(2)),'C1')[0]
z=solution.subs('C1',C1)
print("Частное решение:")
print(z)
print("Ответ сошелся")
print("-----------------------------------------------------------------")
print("#Дифференциальные уравнения первого порядка.")
print("#Дифференциальные уравнения с разделяющимися переменными")
print("#Пример 6")

x = sympy.symbols('x')
y = sympy.Function('y')

equation = sympy.Eq((y(x).diff(x))*y(x)*sympy.sqrt(1-x*x)-sympy.sqrt(3+y(x)*y(x)),0)
print("Условие:")
print(equation)
solution = sympy.dsolve(equation)
print("Общее решение:")
print(solution)
print("Ответ неизвестен правилен ли")
print("-----------------------------------------------------------------")
print("#Дифференциальные уравнения первого порядка.")
print("#Дифференциальные уравнения с разделяющимися переменными")
print("#Пример 7")

x = sympy.symbols('x')
y = sympy.Function('y')

equation = sympy.Eq((y(x).diff(x))*2*(y(x)*x+y(x))+x*(y(x)**4+1),0)
print("Условие:")
print(equation)
solution = sympy.dsolve(equation)
print("Общее решение:")
print(solution)
print("Ответ неизвестен правилен ли")
print("-----------------------------------------------------------------")
print("#Дифференциальные уравнения первого порядка.")
print("#Дифференциальные уравнения с разделяющимися переменными")
print("#Пример 8")

x = sympy.symbols('x')
y = sympy.Function('y')

equation = sympy.Eq(2*y(x).diff(x)*sympy.sin(y(x))*sympy.cos(y(x))*sympy.sin(x)**2+sympy.cos(x),0)
print("Условие:")
print(equation)
solution = sympy.dsolve(equation)
print("Общее решение:")
print(solution)
print("Ответ неизвестен правилен ли")
print("-----------------------------------------------------------------")
for sol in solution:
    C1=sympy.solve(sol.subs(x,sympy.pi*0.5).subs(y(sympy.pi*0.5),0),'C1')
    if C1==[] or C1==[-1]:
        continue
    z=sol.subs('C1',C1)
    print("Частное решение:")
    print(z)

print("# Дифференциальные уравнения первого порядка.")
print("# Дифференциальные уравнения с разделяющимися переменными")
print("# Пример 9")
x = sympy.symbols('x')
y = sympy.Function('y')
equation = sympy.Eq(y(x).diff(x) * y(x) *(1+sympy.exp(x)) - sympy.exp(y(x)), 0)
print("Условие:")
print(equation)
solution = sympy.dsolve(equation)
print("Общее решение:")
print(solution)
print("Ответ неизвестен правилен ли")
print("-----------------------------------------------------------------")
print("# Дифференциальные уравнения первого порядка.")
print("# Дифференциальные уравнения с разделяющимися переменными")
print("# Пример 10")
x = sympy.symbols('x')
y = sympy.Function('y')
print("Условие:")
equation = sympy.Eq(y(x).diff(x) * (x+3*x*x) -y(x)+3, 0)
print(equation)
solution = sympy.dsolve(equation)
print("Общее решение:")
print(solution)
print("Ответ сошелся")
print("-----------------------------------------------------------------")
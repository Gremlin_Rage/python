import math
f = open('example.obj','w')
try:
	f.write("o sphere \n")
	x=float()
	y=float()
	z=float()
	r=0.5
	count = 0;
	i=float(0.0)
	while i<=3.14:
		j=float(0.0)
		while j<2.0 * 3.14:
			x = float(r) * float(math.sin(i)) * float(math.cos(j))
			y = float(r) * float(math.sin(i)) * float(math.sin(j))
			z = float(r) * float(math.cos(i))
			j+=0.01
			f.write("v "+str(x)+" "+str(y)+" "+str(z)+" 1.0 0.0 0.0\n")
		i+=0.01
finally:
	f.close()
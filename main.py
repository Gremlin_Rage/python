# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import csv

import pymysql.cursors

import sys
from PyQt6.QtWidgets import QApplication, QWidget, QTableWidget, QTableWidgetItem, QVBoxLayout, \
    QPushButton, QTextEdit, QLabel
from PyQt6.QtCore import pyqtSlot

a=[]
connection = 0


class Connection():
    def __init__(self):
        b = 0

    @staticmethod
    def connect(hostnameedit, usernameedit, passwordedit, databaseedit, charsetedit):
        # Use a breakpoint in the code line below to debug your script.
        # Подключиться к базе данных.
        global connection

        connection = pymysql.connect(host=hostnameedit.toPlainText(),
                                     user=usernameedit.toPlainText(),
                                     password=passwordedit.toPlainText(),
                                     db=databaseedit.toPlainText(),
                                     charset=charsetedit.toPlainText(),
                                     cursorclass=pymysql.cursors.DictCursor)

        print("connect successful!!")

    @staticmethod
    def query(sqledit):
        try:

            with connection.cursor() as cursor:

                # SQL
                sql = sqledit.toPlainText()  # "SELECT * FROM `users`"

                # Выполнить команду запроса (Execute Query).
                cursor.execute(sql)

                print("cursor.description: ", cursor.description)

                print()
                for row in cursor:
                    print(row)
                    a.insert(len(a), row)
        except:
            print("Can't execute query")

    @staticmethod
    def close():
        # Закрыть соединение (Close connection).
        connection.close()
con = Connection()
class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'Users'
        self.left = 0
        self.top = 0
        self.width = 1000
        self.height = 700

        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        # Add box layout, add table to box layout and add box layout to widget
        self.layout = QVBoxLayout()
        self.labelhost = QLabel("ip address remote host")
        self.labelhost.move(200, 25)
        self.layout.addWidget(self.labelhost)
        self.hostnameedit = QTextEdit()
        self.hostnameedit.acceptRichText()
        self.hostname = "localhost"
        self.hostnameedit.setText(self.hostname)
        self.hostnameedit.setFixedSize(200, 25)
        self.layout.addWidget(self.hostnameedit)

        self.labeluser = QLabel("username")
        self.labeluser.move(200, 25)
        self.layout.addWidget(self.labeluser)
        self.usernameedit = QTextEdit()
        self.usernameedit.acceptRichText()
        self.username = "root"
        self.usernameedit.setText(self.username)
        self.usernameedit.setFixedSize(200, 25)
        self.layout.addWidget(self.usernameedit)

        self.labelpass = QLabel("password")
        self.labelpass.move(200, 25)
        self.layout.addWidget(self.labelpass)
        self.passwordedit = QTextEdit()
        self.passwordedit.acceptRichText()
        self.password = "root"
        self.passwordedit.setText(self.password)
        self.passwordedit.setFixedSize(200, 25)
        self.layout.addWidget(self.passwordedit)

        self.labeldb = QLabel("database")
        self.labeldb.move(200, 25)
        self.layout.addWidget(self.labeldb)
        self.databaseedit = QTextEdit()
        self.databaseedit.acceptRichText()
        self.database = "test"
        self.databaseedit.setText(self.database)
        self.databaseedit.setFixedSize(200, 25)
        self.layout.addWidget(self.databaseedit)

        self.labelchar = QLabel("charset")
        self.labelchar.move(200, 25)
        self.layout.addWidget(self.labelchar)
        self.charsetedit = QTextEdit()
        self.charsetedit.acceptRichText()
        self.charset = "utf8mb4"
        self.charsetedit.setText(self.charset)
        self.charsetedit.setFixedSize(200, 25)
        self.layout.addWidget(self.charsetedit)

        self.buttonc = QPushButton('Connect to database', self)
        self.buttonc.setToolTip('This is a connecting button')
        #self.buttonc.move(500, 170)
        self.layout.addWidget(self.buttonc)
        self.buttonc.clicked.connect(self.buttonClicked)

        self.buttoncc = QPushButton('Close connect', self)
        self.buttoncc.setToolTip('This is a close connection button')
        #self.buttoncc.move(600, 170)
        self.layout.addWidget(self.buttoncc)
        self.buttoncc.clicked.connect(self.buttonClicked)

        self.buttonq = QPushButton('Execute query SQL', self)
        self.buttonq.setToolTip('This is a querying button')
        #self.buttonq.move(700, 170)
        self.layout.addWidget(self.buttonq)
        self.buttonq.clicked.connect(self.buttonClicked)

        self.buttone = QPushButton('Exit program', self)
        self.buttone.setToolTip('Exit program')
        #self.buttone.move(700, 170)
        self.layout.addWidget(self.buttone)
        self.buttone.clicked.connect(self.buttonClicked)

        self.labelcmdsql = QLabel("SQL code")
        self.labelcmdsql .move(200, 25)
        self.layout.addWidget(self.labelcmdsql)
        self.sqledit = QTextEdit()
        self.sqlquery = "SELECT * FROM `users`"
        self.sqledit.acceptRichText()

        self.sqledit.setText(self.sqlquery)
        self.sqledit.setFixedSize(500, 50)
        self.layout.addWidget(self.sqledit)

        self.labelresult = QLabel("Result")
        self.labelresult.move(200, 25)
        self.layout.addWidget(self.labelresult)
        self.tableWidget = QTableWidget()
        self.layout.addWidget(self.tableWidget)
        self.setLayout(self.layout)
        # Show widget
        self.show()

    @pyqtSlot()
    def buttonClicked(self):
        sender = self.sender()
        if sender.text() == "Connect to database":
            con.connect(self.hostnameedit, self.usernameedit, self.passwordedit, self.databaseedit, self.charsetedit)
            print('c button click')
        elif sender.text() == "Execute query SQL":
            try:
                con.query(self.sqledit)
                self.tableWidget.setRowCount(len(a))
                self.tableWidget.setColumnCount(len(a[0]))
                labelsTable = ["id", "nickname", "e-mail", "password", "date registration", "ip address", "user-agent",
                               "phone"]
                self.tableWidget.setHorizontalHeaderLabels(labelsTable)

                j = 0
                for row in a:
                    i = 0
                    for value in row.values():
                        self.tableWidget.setItem(j, i, QTableWidgetItem(str(value)))
                        i += 1
                    j += 1

                #self.tableWidget.move(0, 0)
                print('q button click')
            except:
                print("Can't execute query")
        elif sender.text() == 'Close connect':
            con.close()
        elif sender.text() == 'Exit program':
            exit(0)
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec())

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
